Snapdragon Power Measurement Board (PMB) Library
================================================

## Building

1. Clone into the [`sfpro-dev`](https://gitlab.com/mit-acl/fsw/snap-stack/sfpro-dev) or [`sf-dev`](https://gitlab.com/mit-acl/fsw/snap-stack/sf-dev) `workspace` directory.
2. Update submodules: `git submodule update --init --recursive`.
3. Follow appropriate instructions for building and pushing via `adb`: `./build.sh workspace/snap_pmb --load`.

## Implementation Notes

- The [MCP3426 D/A](https://ww1.microchip.com/downloads/en/DeviceDoc/22226a.pdf) has two channels: one or voltage, one for current. Thus, each time we read we must send a config byte to select the correct channel, and then wait about 100 ms before reading.
- A calibration is required to accurately read voltage and current. This calibration needs to be done for each board and can be done using an Arduino (see [sketch](tools/pmb_test.ino)).

## Example Output

### read_pwm

Run `./read_pwm` (in root dir) to see the voltage and current on the terminal:

```bash
# ran while powered via 4S LiPo
read_pmb  V: 16.4436, I: 0.135746
```

### mini-dm

The following example output was obtained via `mini-dm` (i.e., `make mini-dm` from [sf-dev](https://gitlab.com/mit-acl/fsw/snap-stack/sf-dev) or [sfpro-dev](https://gitlab.com/mit-acl/fsw/snap-stack/sfpro-dev)) on an Excelsior 8096 (sfpro).

```bash
[08500/02]  42:10.832  [mcp3426] Successfully opened '/dev/iic-8' and configured for slave 0x68.  0062  mcp3426.c
[08500/02]  42:10.832  [pmb] Successfully initialized.  0030  pmb_imp.c
[08500/02]  42:11.040  [mcp3426] Deinitialized.  0079  mcp3426.c
[08500/02]  42:11.040  [pmb] Closed.  0055  pmb_imp.c
```
