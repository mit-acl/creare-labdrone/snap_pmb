/**
 * @file snap_pmb.h
 * @brief Power Measurement Board API
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 August 2020
 */

#pragma once

#include <cstdint>

namespace acl {

class SnapPMB
{
public:
    SnapPMB();
    SnapPMB(double vcal, double ical);
    ~SnapPMB();

    bool init();
    void close();
    bool read(float& voltage, float& current);
    bool hw_error() const { return hw_error_; }

private:
    double vcal_, ical_; ///< PMB calibration parameters for A/D
    bool initialized_ = false; ///< apm peripheral initialized (needs closing)
    bool hw_error_ = false; ///< cannot communicate with device
};

} // ns acl
