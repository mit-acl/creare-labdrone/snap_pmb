/**
 * @file snap_pmb.cpp
 * @brief Power Measurement Board API
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 August 2020
 */

#include "snap_pmb/snap_pmb.h"

#include "utils.h"
#include "pmb.h"

namespace acl {

SnapPMB::SnapPMB()
: SnapPMB(11.416, 2.598) // calibration values from SN0
{

}

// ----------------------------------------------------------------------------

SnapPMB::SnapPMB(double vcal, double ical)
: vcal_(vcal), ical_(ical)
{

}

// ----------------------------------------------------------------------------

SnapPMB::~SnapPMB()
{
    if (initialized_) close();
}

// ----------------------------------------------------------------------------

bool SnapPMB::init()
{
    // Make sure we can connect to the device
    if (pmb_init(vcal_, ical_) != PMB_SUCCESS) {
        LOG_ERR("Hardware error: cannot initialize DSP-side peripheral.");
        hw_error_ = true;
        return false;
    }

    hw_error_ = false;
    initialized_ = true;
    return true;
}

// ----------------------------------------------------------------------------

void SnapPMB::close()
{
    pmb_close();
    initialized_ = false;
}

// ----------------------------------------------------------------------------

bool SnapPMB::read(float& voltage, float& current)
{
    pmb_read(&voltage, &current);
    return true;  // TODO: return false if it couldn't be read
}

} // ns acl
