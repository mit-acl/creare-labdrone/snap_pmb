/**
 * @file mcp3426.c
 * @brief C API for the MCP3426-based I2C Power Measurement Board
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 August 2020
 */

#include <fcntl.h>
#include <signal.h>
#include <time.h>
#include <dspal_time.h>
#include <sys/ioctl.h>
#include <dev_fs_lib_i2c.h>

#include "utils.h"
#include "mcp3426.h"

static int fd_ = -1;
static uint8_t addr_ = 0x00;

// calibration values that need to be found for each device
static float vcal_ = 11.416;
static float ical_ = 2.598;

static bool _i2c_slave_config(uint8_t addr);
static float _read_voltage();
static float _read_current();
static bool _write(uint8_t * data, uint8_t len);
static uint8_t _read16(uint8_t * buf);

// ----------------------------------------------------------------------------

bool mcp3426_init(uint8_t i2cdevnum, uint8_t addr, float vcal, float ical)
{
  // 
  // Open the requested I2C device
  //

  char dev[256];
  snprintf(dev, sizeof(dev), DEV_FS_I2C_DEVICE_TYPE_STRING "%d", i2cdevnum);
  fd_ = open(dev, O_RDWR);

  if (fd_ == -1) {
    LOG_ERR("[mcp3426] Opening '%s' failed.", dev);
    return false;
  }

  //
  // Configure the I2C device and sensor
  //

  addr_ = addr;

  // save calibration values for this device
  vcal_ = vcal;
  ical_ = ical;

  bool ret = _i2c_slave_config(addr);
  if (!ret) return false;

  LOG_INFO("[mcp3426] Successfully opened '%s' "
           "and configured for slave 0x%x.", dev, addr);

  return true;
}

// ----------------------------------------------------------------------------

void mcp3426_deinit()
{
  if (fd_ == -1) return;

  // close the file descriptor
  close(fd_);

  // indicate that device is closed
  fd_ = -1;

  LOG_INFO("[mcp3426] Deinitialized.");
}

// ----------------------------------------------------------------------------
 
bool mcp3426_read(float * voltage, float * current, uint64_t * time_us)
{
  struct timespec tp;
  clock_gettime(CLOCK_REALTIME, &tp);
  *time_us = tp.tv_sec * 1e6 + tp.tv_nsec * 1e-3;

  *voltage = _read_voltage();
  *current = _read_current();

  return true;
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

static float _read_voltage()
{
  // Configuration Register (set on write, see Table 5-1, p. 18)
  // - ch 2
  // - continuous data conversion
  // - 16 bit resolution
  uint8_t CONFIG = 0b00111000;
  _write(&CONFIG, 1);

  usleep(100000);

  uint8_t data[2];
  _read16(data);

  uint16_t raw = (((uint16_t)data[0]) << 8) | data[1];  // MSB first 
  return (((float)raw)/0x7FFF) * 2.047 * vcal_;
}

// ----------------------------------------------------------------------------

static float _read_current()
{
  // current conversion parameters -- depends on circuit / resistance
  const float ibase = 0.50;
  const float isense = 0.02666;

  // Configuration Register (set on write, see Table 5-1, p. 18)
  // - ch 1
  // - continuous data conversion
  // - 16 bit resolution
  uint8_t CONFIG = 0b00011000;
  _write(&CONFIG, 1);

  usleep(100000);
 
  uint8_t data[2];
  _read16(data);

  uint16_t raw = (((uint16_t)data[0]) << 8) | data[1];  // MSB first 
  return ((((float)raw)/0x7FFF) * 2.047 * ical_ - ibase) / isense;
}

// ----------------------------------------------------------------------------

static bool _i2c_slave_config(uint8_t addr)
{
  struct dspal_i2c_ioctl_slave_config slave_config;
  slave_config.slave_address = addr;
  slave_config.bus_frequency_in_khz = 400;
  slave_config.byte_transer_timeout_in_usecs = 9000;

  bool ret = ioctl(fd_, I2C_IOCTL_CONFIG, &slave_config);

  if (ret != 0) {
    LOG_ERR("[mcp3426] IOCTL slave 0x%x failed.", addr);
    return false;
  }

  return true;
}

// ----------------------------------------------------------------------------

static bool _write(uint8_t * data, uint8_t len)
{
  uint8_t byte_count = write(fd_, data, len);

  if (byte_count != len) {
    LOG_ERR("[mcp3426] Expected %d bytes written, sent %d", len, byte_count);
    return false;
  }

  return true;
}

// ----------------------------------------------------------------------------

static uint8_t _read16(uint8_t * buf)
{

  uint8_t byte_count = read(fd_, buf, 2);

  if (byte_count != 2)
    LOG_ERR("[mcp3426] Expected 2 byte received, got %d", byte_count);

  return byte_count;
}
