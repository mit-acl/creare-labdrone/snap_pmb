/**
 * @file mcp3426.h
 * @brief C API for the MCP3426-based I2C Power Measurement Board
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 August 2020
 */

#pragma once

#include <unistd.h>
#include <stdint.h>
#include <stdbool.h>

bool mcp3426_init(uint8_t i2cdevnum, uint8_t addr, float vcal, float ical);
void mcp3426_deinit();
bool mcp3426_read(float * voltage, float * current, uint64_t * time_us);
