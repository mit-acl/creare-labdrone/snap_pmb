/**
 * @file pmb_imp.c
 * @brief DSP implementation to read PMB via I2c
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 August 2020
 */

#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <sys/ioctl.h>
#include <dev_fs_lib_i2c.h>

#include "utils.h"
#include "mcp3426.h"

#define MCP3426_I2C_ADDRESS 0b01101000

static void i2c_sniff(int i2cdevnum);

// ----------------------------------------------------------------------------

int pmb_init(float vcal, float ical)
{
  // Open /dev/iic-# (note: using BLSP, not SSC).
  const uint8_t i2c_devnum = 8; // This is J1 on APQ8096
  bool success = mcp3426_init(i2c_devnum, MCP3426_I2C_ADDRESS, vcal, ical);
  if (!success) return PMB_ERROR;

  LOG_INFO("[pmb] Successfully initialized.");

  // i2c_sniff(8);

  return PMB_SUCCESS;
}

// ----------------------------------------------------------------------------

int pmb_read(float* voltage, float* current)
{
  uint64_t time_us;
  mcp3426_read(voltage, current, &time_us);

  //LOG_INFO("[pmb] V: %.2f\tI: %.2f\tt: %llu", *voltage, *current, time_us);

  return PMB_SUCCESS;
}

// ----------------------------------------------------------------------------

void pmb_close(void)
{
  mcp3426_deinit();

  LOG_INFO("[pmb] Closed.");
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

static void i2c_sniff(int i2cdevnum)
{

  // open I2C device
  char dev[256];
  snprintf(dev, sizeof(dev), DEV_FS_I2C_DEVICE_TYPE_STRING "%d", i2cdevnum);
  int fd = open(dev, O_RDWR);

  for (uint8_t i=0; i<127; ++i) 
  {
    // Configure I2C with slave address
    struct dspal_i2c_ioctl_slave_config slave_config;
    slave_config.slave_address = i;
    slave_config.bus_frequency_in_khz = 400;
    slave_config.byte_transer_timeout_in_usecs = 9000;
    ioctl(fd, I2C_IOCTL_CONFIG, &slave_config);

    // query for device on this bus with addr i
    uint8_t data = 0;
    uint8_t len = write(fd, &data, 1);
    if (len == 1) {
      LOG_INFO("0x%x: Device found. Sent %d bytes", i, len);
    }

    usleep(100000);
  }

  // close I2C device
  close(fd);

}
