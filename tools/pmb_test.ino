//  Lab Drone II
//  Power Monitoring Board Shakedown Test
//  CZiegler
//  14 JULY 20

//  This code was used for basic validation of the power monitoring board.
//  The power monitoring board communicates with the master using I2C.
//  Using a lab bench setup, provide a constant and known power through the power monitoring board.

//  To calibrate for voltage:
//  1) Set a known volage to the input of the board from a calibrated source.
//  2) Trim v_cal until the measurement matches the source voltage.

//  To calibrate for current:
//  1) Draw a known amount of current through the board from a calibrated load.
//  2) Trim a_cal until the measurement matches the sunk current.

#include <Wire.h>

void setup()
{
  Wire.begin(); //  Join I2C bus as master
  Serial.begin(9600);
}

void loop()
{
  while (!Serial); //  Wait for serial monitor
  Serial.println("\nI2C Scanner");

  byte error, address;
  uint8_t device_address = 104, num_bytes = 2;
  int nDevices, v_reading, i_reading;
  float voltage = 0.0, current = 0.0;

  //  calibration value for SN 0
  float v_cal = 11.416;
  //  calibration value for SN 1
  //float v_cal = 11.358;



  float a_sense = .02666;
  float a_base = .50;
  //  calibration value for SN 0
  float a_cal = 2.598;
  //  calibration value for SN1
  //float a_cal = 2.587;



//  Serial.println("Scanning...");
//
//  nDevices = 0;
//  for (address = 1; address < 127; address++)
//  {
//    //  the i2c_scanner uses the return value of
//    //  the Write.endTransmisstion to see if
//    //  a device did acknowledge to the address.
//    Wire.beginTransmission(address);
//    error = Wire.endTransmission();
//
//
//    if (error == 0)
//    {
//      Serial.print("I2C device found at address 0x");
//      if(address<16)
//        Serial.print("0");
//      Serial.print(address, HEX);
//      Serial.println("  !");
//      device_address = address;
//      nDevices++;
//
//    }
//    else if (error == 4)
//    {
//      Serial.print("Unknown error at address 0x");
//      if(address<16)
//        Serial.print("0");
//      Serial.println(address, HEX);
//    }
//
//  }
//  if (nDevices == 0)
//    Serial.println("No I2C devices found\n");
//  else
  //Query for voltage
  Wire.beginTransmission(device_address); //  address scanned for device
  Wire.write(byte(0x38)); //  set register settings to be continuous measurement, 16bit resolution, channel 2
  Wire.endTransmission(); //  transmit
    delay(500);      // wait 5 seconds

  Wire.requestFrom(device_address,num_bytes); //  receive 16 bits of data
  if (2 <= Wire.available()) // if two bytes were received
  {
    v_reading = Wire.read();  // receive high byte (overwrites previous reading)
    v_reading = v_reading << 8;    // shift high byte to be high 8 bits
    v_reading |= Wire.read(); // receive low byte as lower 8 bits
  }

  voltage = (((float)v_reading)/32767)*2.047*v_cal; //  calculate voltage from counts
  Serial.println("Voltage Measured:");
  Serial.println(voltage);   // print the voltage



  delay(500);      // wait 5 seconds

  //Query for currrent
  Wire.beginTransmission(device_address); //  address scanned for device
  Wire.write(byte(0x18)); //  set register settings to be continuous measurement, 16bit resolution, channel 1
  Wire.endTransmission(); //  transmit
    delay(500);      // wait 5 seconds

  Wire.requestFrom(device_address,num_bytes); // receive 16 bits of data
  if (2 <= Wire.available()) // if two bytes were received
  {
    i_reading = Wire.read();  // receive high byte (overwrites previous reading)
    i_reading = i_reading << 8;    // shift high byte to be high 8 bits
    i_reading |= Wire.read(); // receive low byte as lower 8 bits
  }
      delay(500);      // wait 5 seconds

  current = ((((float)i_reading)/32767)*2.047*a_cal-a_base)/a_sense;
  Serial.println("Current Measured:");
  Serial.println(current);   // print the current


  delay(5000);           // wait 5 seconds
}