/**
 * @file read_pmb.cpp
 * @brief CPU entry point for querying power measurement board
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 August 2020
 */

#include <iostream>

#include "snap_pmb/snap_pmb.h"


int main(int argc, char const *argv[])
{
  acl::SnapPMB pmb;
  pmb.init();

  float voltage, current;
  pmb.read(voltage, current);
  std::cout << "read_pmb  V: " << voltage << ", I: " << current << std::endl;

  pmb.close();

  return 0;
}
